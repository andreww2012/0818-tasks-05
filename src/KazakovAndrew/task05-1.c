﻿/*
	Написать программу, в которой создается структура BOOK для хранения информации о названиях книг, ФИО авторов и годе издания. Из текстового файла, подготовленного в обычном редакторе, считываются данные о книгах в динамический массив структур BOOK.
	Распечатать данные о книгах на экране
	Распечатать данные о самой старой и самой новой книге (по году издания)
	Распечатать данные, отсортированные по фамилиям авторов

	Формат входных данных:
	Название;Фамилия <Имя и/или отчество в любом формате>;ГГГГ
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#define FILENAME		"data.txt"
#define FIELD_LEN		200 // Максимальная длина полей
#define STR_LEN			FIELD_LEN * 2 + 6 // Максимальная длина строки (6 - разделители и год)
#define FIELDS_AMOUNT	3
#define MAX_YEAR		2050

struct BOOK {
	char name[FIELD_LEN];
	char author[FIELD_LEN];
	int year;
};

void get_book(char *str, struct BOOK *bk) {

	int i, j, k;
	int temp_int;
	char temp[FIELD_LEN];

	j = k = 0;
	for (i = 1; i <= FIELDS_AMOUNT; i++) {
		while (str[j] != ';' && str[j] != '\n') {
			temp[k] = str[j];
			j++;
			k++;
		}
		temp[k] = '\0';
		k = 0; // Обнуление позиции temp-строки
		j++; // Обязательно переходим к следующему символу

		switch (i) {
			case 1:
				strcpy(bk->name, temp);
			case 2:
				strcpy(bk->author, temp);
			case 3:
				bk->year = ((temp_int = atoi(temp)) <= MAX_YEAR) ? temp_int : MAX_YEAR;
		}
	} // Конец for

}

int compare_books(const void *a, const void *b) {
	return strcmp(
		(*(struct BOOK**)a)->author,
		(*(struct BOOK**)b)->author
	);
}


int main() {

	FILE *f;
	struct BOOK **books; // Сделано так, т. к. потом будем сортировать указатели, а не сами массивы
	int i, N;
	char temp[STR_LEN];
	int oldest_book;
	int newest_book;

	setlocale(LC_ALL, "rus");

	f = fopen(FILENAME, "rt");

	if (f) {

		N = 0;
		while (fgets(temp, STR_LEN, f)) {
			if (temp[0] != '\n') {
				N++;
			}
		}

		rewind(f);

		books = (struct BOOK**)malloc(N * sizeof(struct BOOK*));
		for (i = 0; i < N; i++) {
			books[i] = (struct BOOK*)malloc(sizeof(struct BOOK));
		}

		i = 0;
		while (fgets(temp, STR_LEN, f)) {
			if (temp[0] != '\n') {
				get_book(temp, books[i++]);
			}
		}

		// Печать данных о книгах. Нахождение самой старой и самой новой и печать информации о них
		oldest_book = newest_book = 0;
		puts("Список всех книг:");
		for (i = 0; i < N; i++) {
			if (books[i]->year > books[newest_book]->year) {
				newest_book = i;
			}
			if (books[i]->year < books[oldest_book]->year) {
				oldest_book = i;
			}
			printf("%s - %s (%d)\n", books[i]->name, books[i]->author, books[i]->year);
		}

		puts("\nСамая старая книга:");
		printf("%s - %s (%d)\n", books[oldest_book]->name, books[oldest_book]->author, books[oldest_book]->year);

		puts("\nСамая новая книга:");
		printf("%s - %s (%d)\n", books[newest_book]->name, books[newest_book]->author, books[newest_book]->year);

		// Данные о всех книгах, отсортированные по фамилиям авторов
		qsort(books, N, sizeof(struct BOOK*), compare_books);
		puts("\nОтсортированные по авторам книги:");
		for (i = 0; i < N; i++) {
			printf("%s - %s (%d)\n", books[i]->name, books[i]->author, books[i]->year);
		}

	} else {
		printf("При открытии файла %s произошла ошибка:\n", FILENAME);
		perror("");

		exit(1);
	}

	return 0;
}